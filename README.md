Bolierplate for Re:VIEW
=======================

What is this?
-------------

This toolkit is a boilerplate for writing books by [Re:VIEW](https://github.com/kmuto/review).

Requirements
------------

  * ruby
  * bundler
  * compiler environment for rubygem's native extensions.

How to use
----------

```
# checkout this repository
$ git clone https://github.com/nyarla/review-boilerplate.git my-great-book
$ cd my-great-book

# install dependences tools
$ bundle install

# starting watcher and httpd html viewer by foreman
$ rake start
```

See Also
--------

  * [Re:VIEW](https://github.com/kmuto/review)

Author
------

Naoki OKAMURA (nyarla) <nyarla@thotep.net>

Unlicensed
----------

[CC0 (Public Domain Dedication)](https://creativecommons.org/publicdomain/zero/1.0/)
